# How to

## Prerequisites
* Java JRE 1.8
* A *nix platform

# Prepare environment
Just lanch `init.sh` file. It downloads jmeter v5.0, extracts it and removes archive.


# Configurations
`jmeter.properties` contains two parts for end user configrations *Page address configuration* and *configuration of requests count*.

`configuration of requests count` contains:
* loop - How many test executions the user will done.
* NoUsers - how much user will be emultated.
* RampUp - How fast all users appears.

Examples:
* `RampUp=0` - means all users start perform test at the same time. 
* `RampUp=3600` means all users appears during the hour. 
* `RampUp=100, NoUsers=100, loop=1` - 100 simulated users perfrom one cycle of requests. Every user starts 1 second after the previous one. 

# Launch 
Execute `start.sh` file.

# Results
Test run generates all results in `results` folder
* `jmeter.log` - main log file useful for debugging
* `jmeter.csv` - csv file of test run
* `html-report/index.html` - human redable html-format report.

Note:
Results are clened before every test run.
