#!/bin/sh
rm -r jmeter
curl -LO https://archive.apache.org/dist/jmeter/binaries/apache-jmeter-5.0.tgz
mkdir jmeter
tar -xzf apache-jmeter-5.0.tgz -C jmeter --strip 1
rm -f apache-jmeter-5.0.tgz
