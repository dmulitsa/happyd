#!/bin/sh
RESULTS_DIR=results
HTML_DIR=$RESULTS_DIR/html-report
props=jmeter.properties

if [ ! -d "$RESULTS_DIR" ]; then
	mkdir results
fi
if [ ! -d "$HTML_DIR" ]; then
	mkdir $HTML_DIR
fi
rm -r results/*
./jmeter/bin/jmeter -n \
	-j results/jmeter.log \
	-t main-page.jmx \
	-p $props \
	-l results/jmeter.csv -e -o results/html-report
